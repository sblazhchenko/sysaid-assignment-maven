package com.sysaid.assignment.service;

import com.sysaid.assignment.dao.RatingDao;
import com.sysaid.assignment.dao.TaskDao;
import com.sysaid.assignment.dao.TaskHistoryDao;
import com.sysaid.assignment.dao.WishListDao;
import com.sysaid.assignment.domain.Task;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.util.Collections.reverseOrder;
import static java.util.stream.Collectors.toList;

@Service
public class TaskServiceImpl implements TaskService {
    @Value("${external.boredapi.baseURL}")
    private String baseUrl;

    private final TaskDao taskDao;
    private final RatingDao ratingDao;
    private final WishListDao wishListDao;
    private final TaskHistoryDao taskHistoryDao;

    private final static Random RANDOM = new Random();
    private final static int TO_WISH_LIST_RATING = 1;
    private final static int COMPLETE_RATING = 2;

    public TaskServiceImpl(TaskDao taskDaoInMemory, // or TaskDao taskDaoSynchronized,
                           RatingDao ratingDaoImMemory,
                           WishListDao wishListDaoImMemory,
                           TaskHistoryDao taskHistoryDaoInMemory,
                           @Value("${external.boredapi.baseURL}") String baseUrl) {
        this.taskDao = taskDaoInMemory;
        this.ratingDao = ratingDaoImMemory;
        this.wishListDao = wishListDaoImMemory;
        this.taskHistoryDao = taskHistoryDaoInMemory;
        this.baseUrl = baseUrl;
    }

    //TODO: Add tests.
    //TODO: Add synchronization for all methods
    //TODO: Add global exception handler. For example, if remote API is not available. Or add retry mechanism.
    //TODO: Add logs. For example, log all requests to remote API.

    @Override
    public Task[] getUncompletedTasks(String user, String type) {
        //TODO: change to use task type for getting tasks
        //TODO: change to use tasks 80% from wish list and 20% from remote API
        //TODO: extend remote API to support get list of task types. Then use it to get tasks by type.

        List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Task task = getInitTask();
            tasks.add(task);
        }
        return tasks.toArray(new Task[0]);
    }

    //TODO: Clarify! Will we get tasks from personal wish list? If yes, what is the probabilities?
    @Override
    public Task getRandomTask(String user) {
        int sizeOfAllTasks = taskDao.getAllTasks().size();
        int numberOfShownTasks = taskHistoryDao.getNumberOfShownTasksOfTheDay(user);
        // if all tasks are shown or there is no tasks, then get one from remote API
        if (sizeOfAllTasks == 0 || sizeOfAllTasks == numberOfShownTasks) {
            // if all tasks are shown, then init new one
            while (true) {
                Task initTask = getInitTask();
                if (!taskHistoryDao.isTaskShown(user, initTask.getKey())) {
                    taskHistoryDao.addShownTaskOfTheDay(user, initTask.getKey());
                    return initTask;
                }
            }
        }
        // if not all tasks are shown, then get random task
        while (true) {
            Task task = getRandomTaskImpl();
            if (!taskHistoryDao.isTaskShown(user, task.getKey())) {
                taskHistoryDao.addShownTaskOfTheDay(user, task.getKey());
                return task;
            }
        }
    }

    @Override
    public void completeTask(String taskId) {
        if (taskDao.ifTaskPresent(taskId)) {
            ratingDao.increaseRating(taskId, COMPLETE_RATING);
        }
    }

    @Override
    public void addToWishList(String user, String taskId) {
        if (taskDao.ifTaskPresent(taskId)) {
            wishListDao.addToWishList(user, taskId);
            ratingDao.increaseRating(taskId, TO_WISH_LIST_RATING);
        }
    }

    @Override
    public Task[] getWishList(String user) {
        List<String> wishList = wishListDao.getWishList(user);
        return wishList.stream()
                .map(taskDao::findTask)
                .toArray(Task[]::new);
    }

    private Task getTask() {
        String endpointUrl = String.format("%s/activity", baseUrl);
        RestTemplate template = new RestTemplate();
        ResponseEntity<Task> responseEntity = template.getForEntity(endpointUrl, Task.class);
        return responseEntity.getBody();
    }

    private Task getInitTask() {
        Task task = getTask();
        taskDao.addTask(task);
        ratingDao.addTask(task);
        return task;
    }

    private Task getRandomTaskImpl() {
        int chance = RANDOM.nextInt(100);

        if (chance < 20) {
            return selectRandomFromTopRated(1);
        } else if (chance < 40) {
            return selectRandomFromTopRated(2);
        } else if (chance < 50) {
            return selectRandomFromTopRated(3);
        } else if (chance < 55) {
            return selectRandomFromTopRated(4);
        } else if (chance < 60) {
            return selectRandomFromTopRated(5);
        }

        int index = RANDOM.nextInt(taskDao.getAllTasks().size());
        return taskDao.getTackByIndex(index);
    }

    private Task selectRandomFromTopRated(int idOfTop) {
        List<String> taskIdsFromTop = getTaskIdsFromTop(idOfTop);

        // get random task id from tasks top idOfTop
        int index = RANDOM.nextInt(taskIdsFromTop.size());
        String randomTaskId = taskIdsFromTop.get(index);

        if (taskDao.ifTaskPresent(randomTaskId)) {
            return taskDao.findTask(randomTaskId);
        }
        return getInitTask();
    }

    private List<String> getTaskIdsFromTop(int i) {
        Map<String, Integer> allRatings = ratingDao.getAllRatings();
        // get rating of i-th task
        Integer rating = new HashSet<>(allRatings.values())
                .stream()
                .sorted(reverseOrder())
                .skip(i - 1)
                .findFirst()
                .orElse(0);

        // get all task ids with rating of i-th task
        return allRatings.entrySet()
                .stream()
                .filter(entry -> entry.getValue().equals(rating))
                .map(Map.Entry::getKey)
                .collect(toList());
    }

}
