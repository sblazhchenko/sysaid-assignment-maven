package com.sysaid.assignment.service;

import com.sysaid.assignment.domain.Task;

public interface TaskService {

    Task[] getUncompletedTasks(String user, String type);

    Task getRandomTask(String user);

    void completeTask(String taskId);

    void addToWishList(String user, String taskId);

    Task[] getWishList(String user);
}
