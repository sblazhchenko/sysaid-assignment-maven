package com.sysaid.assignment.controller;

import com.sysaid.assignment.domain.Task;
import com.sysaid.assignment.service.TaskServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * the controller is a basic structure and save some time on "dirty" work.
 */
@RestController
public class TaskController {
    private final TaskServiceImpl taskService;

    public TaskController(TaskServiceImpl taskService) {
        this.taskService = taskService;
    }

    /**
     * will return uncompleted tasks for given user
     *
     * @param user the user which the tasks relevant for
     * @param type type of the task
     * @return list uncompleted tasks for the user
     */
    @GetMapping("/uncompleted-tasks/{user}")
    public ResponseEntity<Task[]> getUncompletedTasks(@PathVariable("user") String user,
                                                      @RequestParam(name = "type", required = false) String type) {
        Task[] raTask = taskService.getUncompletedTasks(user, type);
        return ResponseEntity.ok(raTask);
    }

    /**
     * example for simple API use
     *
     * @return random task of the day
     */
    @GetMapping("/task-of-the-day/{user}")
    public ResponseEntity<Task> getTaskOfTheDay(@PathVariable("user") String user) {
        Task randomTask = taskService.getRandomTask(user);
        return ResponseEntity.ok(randomTask);
    }

    @GetMapping("/wish-list/{user}")
    public ResponseEntity<Task[]> getWishList(@PathVariable("user") String user) {
        Task[] wishList = taskService.getWishList(user);
        return ResponseEntity.ok(wishList);
    }

    /**
     * will complete the task and add rating to the task
     *
     * @param taskId the task to complete
     */
    //TODO: change to POST
    @GetMapping("/completeTask/{taskId}")
    public void completeTask(@PathVariable("taskId") String taskId) {
        taskService.completeTask(taskId);
    }

    /**
     * will add the task to the wish list and add rating to the task
     *
     * @param taskId the task to add to the wish list
     */
    //TODO: change to POST
    @GetMapping("/addToWishList/{user}/{taskId}")
    public void addToWishList(@PathVariable("user") String user,
                              @PathVariable("taskId") String taskId) {
        taskService.addToWishList(user, taskId);
    }

}
