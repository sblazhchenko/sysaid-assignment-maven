package com.sysaid.assignment.dto;

import java.util.List;

// Will be used in the future when DB will be added
public class WishListDto {
    private String userId;
    private List<String> taskIds;
}
