package com.sysaid.assignment.dto;

import java.time.LocalDateTime;

// Will be used in the future when DB will be added
public class TaskHistoryDto {
    private String taskId;
    private String action;
    private LocalDateTime timestamp;
}
