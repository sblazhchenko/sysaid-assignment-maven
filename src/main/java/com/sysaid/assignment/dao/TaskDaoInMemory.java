package com.sysaid.assignment.dao;

import com.sysaid.assignment.domain.Task;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Repository
public class TaskDaoInMemory implements TaskDao {
    private final Set<Task> tasks;

    public TaskDaoInMemory() {
        this.tasks = new HashSet<>();
    }

    @Override
    public void addTask(Task task) {
        this.tasks.add(task);
    }

    @Override
    public Set<Task> getAllTasks() {
        return this.tasks;
    }

    @Override
    public Set<Task> getTasksByType(String type) {
        return this.tasks.stream()
                .filter(task -> task.getType().equals(type))
                .collect(toSet());
    }

    @Override
    public Task findTask(String key) {
        return this.tasks.stream()
                .filter(task -> task.getKey().equals(key))
                .findFirst().orElse(null);
    }

    @Override
    public boolean ifTaskPresent(String key) {
        return this.tasks.stream()
                .anyMatch(task -> task.getKey().equals(key));
    }

    @Override
    public Task getTackByIndex(int i) {
        return tasks.toArray(new Task[0])[i];
    }
}
