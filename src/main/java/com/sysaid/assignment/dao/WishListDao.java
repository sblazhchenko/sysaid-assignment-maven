package com.sysaid.assignment.dao;

import java.util.List;

public interface WishListDao {
    void addToWishList(String userId, String taskId);

    List<String> getWishList(String user);
}
