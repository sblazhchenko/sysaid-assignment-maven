package com.sysaid.assignment.dao;

public interface TaskHistoryDao {
    void addShownTaskOfTheDay(String user, String taskId);

    boolean isTaskShown(String user, String taskId);

    int getNumberOfShownTasksOfTheDay(String user);
}
