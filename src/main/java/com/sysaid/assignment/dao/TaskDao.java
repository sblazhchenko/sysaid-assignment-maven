package com.sysaid.assignment.dao;

import com.sysaid.assignment.domain.Task;

import java.util.Set;

public interface TaskDao {
    void addTask(Task task);

    Set<Task> getAllTasks();

    Set<Task> getTasksByType(String type);

    Task findTask(String key);

    boolean ifTaskPresent(String key);

    Task getTackByIndex(int i);
}
