package com.sysaid.assignment.dao;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

@Repository
public class TaskHistoryDaoInMemory implements TaskHistoryDao {
    private final Map<String, List<String>> shownTaskOfTheDay;

    public TaskHistoryDaoInMemory() {
        shownTaskOfTheDay = new HashMap<>();
    }

    @Override
    public void addShownTaskOfTheDay(String user, String taskId) {
        if (!shownTaskOfTheDay.containsKey(user)) {
            shownTaskOfTheDay.put(user, new ArrayList<>());
        }
        shownTaskOfTheDay.get(user).add(taskId);
    }

    @Override
    public boolean isTaskShown(String user, String taskId) {
        return shownTaskOfTheDay.getOrDefault(user, emptyList()).contains(taskId);
    }

    @Override
    public int getNumberOfShownTasksOfTheDay(String user) {
        return shownTaskOfTheDay.getOrDefault(user, emptyList()).size();
    }
}
