package com.sysaid.assignment.dao;

import com.sysaid.assignment.domain.Task;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RatingDaoImMemory implements RatingDao {
    private final Map<String, Integer> ratings;

    public RatingDaoImMemory() {
        ratings = new HashMap<>();
    }

    @Override
    public void addTasks(List<Task> tasks) {
        tasks.forEach(this::addTask);
    }

    @Override
    public void addTask(Task task) {
        ratings.put(task.getKey(), 0);
    }

    @Override
    public Map<String, Integer> getAllRatings() {
        return ratings;
    }

    @Override
    public void increaseRating(String taskId, int addRating) {
        ratings.put(taskId, ratings.get(taskId) + addRating);
    }
}
