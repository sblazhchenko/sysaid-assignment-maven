package com.sysaid.assignment.dao;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

@Repository
public class WishListDaoImMemory implements WishListDao {
    private final Map<String, List<String>> wishLists;

    public WishListDaoImMemory() {
        wishLists = new HashMap<>();
    }

    @Override
    public void addToWishList(String userId, String taskId) {
        wishLists.computeIfAbsent(userId, v -> new ArrayList<>()).add(taskId);
    }

    @Override
    public List<String> getWishList(String user) {
        return wishLists.getOrDefault(user, emptyList());
    }

}
