package com.sysaid.assignment.dao;

import com.sysaid.assignment.domain.Task;

import java.util.List;
import java.util.Map;

public interface RatingDao {
    void addTasks(List<Task> tasks);

    void addTask(Task task);

    Map<String, Integer> getAllRatings();

    void increaseRating(String taskId, int addRating);
}
